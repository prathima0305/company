package com.prathima.company.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.prathima.company.model.CompanyTransaction;

public interface CompanyTransactionRepository extends MongoRepository<CompanyTransaction, Integer>{
	//@Query(value="{'companyTransaction.transactionDate' : {$gt : ?0, $lt : ?1}}")
	//Optional<List<Company>> findTransactionsByQuery(Date startDate, Date endDate);
	
	Optional<List<CompanyTransaction>> findByRegistrationIdAndTransactionDateBetween(String regId, Date startDate, Date endDate);
}
