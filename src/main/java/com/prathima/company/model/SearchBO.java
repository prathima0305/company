package com.prathima.company.model;

import java.io.Serializable;

public class SearchBO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String registrationId;
	private String startDate;
	private String endDate;

	public SearchBO() {
	}

	public SearchBO(String registrationId, String startDate, String endDate) {
		super();
		this.registrationId = registrationId;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
