package com.prathima.company.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

public class CompanyTransaction {
	@Id
	private String id;
	private String registrationId;
	private String merchantName;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date transactionDate;
	private String desc;

	public CompanyTransaction() {
	}

	public CompanyTransaction(String registrationId, String merchantName, Date transactionDate, String desc) {
		super();
		this.registrationId = registrationId;
		this.merchantName = merchantName;
		this.transactionDate = transactionDate;
		this.desc = desc;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
