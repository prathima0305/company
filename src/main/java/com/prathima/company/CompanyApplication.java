package com.prathima.company;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.prathima.company.model.Company;
import com.prathima.company.model.CompanyTransaction;
import com.prathima.company.repository.CompanyRepository;
import com.prathima.company.repository.CompanyTransactionRepository;
import com.prathima.company.repository.LogRepository;

@SpringBootApplication
public class CompanyApplication implements CommandLineRunner{
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CompanyTransactionRepository companyTransactionRepository;
	
	@Autowired
	private LogRepository logRepository;

	public static void main(String[] args) {
		SpringApplication.run(CompanyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		companyRepository.deleteAll();
		companyTransactionRepository.deleteAll();
		logRepository.deleteAll();
		
		companyRepository.save(new Company("REG01", "Company1"));
		companyRepository.save(new Company("REG02", "Company2"));
		
		companyTransactionRepository.save(new CompanyTransaction("REG01", "Merchant1", new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2020"), "Merchant1 Transaction on debit"));
		companyTransactionRepository.save(new CompanyTransaction("REG02", "Merchant2", new SimpleDateFormat("dd/MM/yyyy").parse("01/03/2020"), "Merchant2 Transaction on credit payment"));
	}
	
}
