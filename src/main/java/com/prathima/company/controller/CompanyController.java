package com.prathima.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prathima.company.model.Company;
import com.prathima.company.model.CompanyTransaction;
import com.prathima.company.model.SearchBO;
import com.prathima.company.service.CompanyService;

@RestController
@RequestMapping("/company")
public class CompanyController {
	
	@Autowired
	private CompanyService companyService;
	
	/**
	 * API to retrieve all master details of a company
	 * */
	@GetMapping("/find/all")
	public ResponseEntity<List<Company>> getAllCompanyDetails(){
		return ResponseEntity.ok().body(companyService.findAllCompanies());
	}
	
	/**
	 * API to retrieve company details by registration id (like)
	 * */
	@GetMapping("/find/{registrationId}")
	public ResponseEntity<List<Company>> getCompanyDetails(@PathVariable String registrationId){
		return ResponseEntity.ok().body(companyService.getCompanyDetails(registrationId));
	}
	
	/**
	 * API to retrieve transaction details based on registration id, start date, end date
	 * */
	@PostMapping("/trans/search")
	public ResponseEntity<List<CompanyTransaction>> getCompanyTransactionDetails(@RequestBody SearchBO search) throws Exception{
		return ResponseEntity.ok().body(companyService.getCompanyTransactions(search));
	}
	
	/**
	 * API to add the master data into Company Collection.
	 * */
	@PostMapping("/add")
	public ResponseEntity<String> saveCompany(@RequestBody Company company) throws Exception{
		return ResponseEntity.ok().body(companyService.saveCompany(company));
	}
	
	/**
	 * API to add the transaction data into Company Transaction Collection.
	 * */
	@PostMapping("/trans/insert")
	public ResponseEntity<String> insertCompanyTrans(@RequestBody CompanyTransaction companyTrans) throws Exception{
		return ResponseEntity.ok().body(companyService.insertCompanyTransaction(companyTrans));
	}
	
	@GetMapping("/hello")
	public String hello(){
		return "Hello!!";
	}
}
